package com.example.hackthonpodkarpacie;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.Set;

public class MenuActivity extends AppCompatActivity {

    BluetoothAdapter mBlueAdapter;
    Intent alarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        alarm = new Intent( this, Alert.class);
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);

        this.registerReceiver(BTReceiver, filter);



        //adapter
        mBlueAdapter = BluetoothAdapter.getDefaultAdapter();

        //check if bluetooth is available or not
        if (mBlueAdapter == null){
            Log.i("bluetooth status" , "Bluetooth is not available");
        }
        else {
            Log.i("bluetooth status", "Bluetooth is available" );
        }

        if (mBlueAdapter.isEnabled()) {
            // mPairedTv.setText("Paired Devices");
            Set<BluetoothDevice> devices = mBlueAdapter.getBondedDevices();
            for (BluetoothDevice device : devices) { // TODO Edycja UI
                //      mPairedTv.append("\nDevice: " + device.getName()+ ", " + device);
                Log.i("deviceName", device.getName());
            }
        } else {
            //bluetooth is off so can't get paired devices
            showToast("Turn on bluetooth to get paired devices");
        }

    }

    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void addPerson(View view) {
        //   Intent intent = new Intent(this, AddUser.class);
        //  startActivity(intent);
        Intent intentOpenBluetoothSettings = new Intent();
        intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
        startActivity(intentOpenBluetoothSettings);
    }
    public void showPerson(View view) {
        Intent intent = new Intent(this, ListofUser.class);
        startActivity(intent);
    }

    public void location(View view) {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }

    //The BroadcastReceiver that listens for bluetooth broadcasts
    final BroadcastReceiver BTReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                //Do something if connected
                Toast.makeText(getApplicationContext(), "BT Connected", Toast.LENGTH_SHORT).show();
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                //Do something if disconnected
                Toast.makeText(getApplicationContext(), "BT Disconnected", Toast.LENGTH_SHORT).show();
                startActivity(alarm);//Device has disconnected
            }

        }


    };

}